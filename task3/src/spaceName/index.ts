import Utilities from './utils';
import Models from './models';

const numbers = [1, 2, 3, 4, 5];
console.log('Original array:', numbers);

const clampedValue = Utilities.clamp(10, 1, 5);
console.log('Clamped value:', clampedValue);

const shuffledArray = Utilities.shuffle(numbers);
console.log('Shuffled array:', shuffledArray);

const average = Utilities.calculateAverage(numbers);
console.log('Average:', average);

const item = new Models.Item<number>('Example', 10, 123);
console.log('Item:', item);
