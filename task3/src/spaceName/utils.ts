namespace Utilities {
    
    export function clamp(value: number, min: number, max: number): number {
        return Math.min(Math.max(value, min), max);
    }

    export function shuffle<T>(array: T[]): T[] {
        let currentIndex = array.length, temporaryValue, randomIndex;

        while (0 !== currentIndex) {
            randomIndex = Math.floor(Math.random() * currentIndex);
            currentIndex -= 1;

            temporaryValue = array[currentIndex];
            array[currentIndex] = array[randomIndex];
            array[randomIndex] = temporaryValue;
        }

        return array;
    }

    export function calculateAverage(array: number[]): number {
        if (array.length === 0) return 0;
        const sum = array.reduce((acc, val) => acc + val, 0);
        return sum / array.length;
    }
}

export default Utilities;