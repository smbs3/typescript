type Restriction =  number | string | object;


class DataStore<T extends Restriction> {
  data: T[] = [];

  addItem(item: T): void {
    this.data.push(item);
  }

  getItem(): T[] {
    return this.data;
  }

  removeItem(id: T): void {
    const index = this.data.findIndex(item => item === id);
    if (index !== -1) {
      this.data.splice(index, 1);
    }
  }

  getItemByIndex(index: number): T | undefined {
    if (index >= 0 && index < this.data.length) {
      return this.data[index];
    } else {
      return undefined;
    }
  }
}





const numberStore = new DataStore<number>();
numberStore.addItem(1);
numberStore.addItem(2);
numberStore.addItem(3);


// const booleanStore = new DataStore<boolean>();
//  booleanStore.addItem(false);//type boolean does n ot satisfy the constraint restriction.

const stringStore = new DataStore<string>();
stringStore.addItem("apple");
stringStore.addItem("banana");
stringStore.addItem("orange");


type Person = {
  name: string;
  age: number;
};


const personStore = new DataStore<Person>();
personStore.addItem({ name: "Alice", age: 30 });
personStore.addItem({ name: "Bob", age: 25 });
personStore.addItem({ name: "Charlie", age: 35 });


console.log("Numbers:", numberStore.getItem());
console.log("Strings:", stringStore.getItem());
console.log("Persons:", personStore.getItem());

