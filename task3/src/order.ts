interface Order {
    id: number;
    product: string;
    quantity: number;
    price: number;
}

interface Physical extends Order {
    deliveryDate: Date;
}

interface Digital extends Order {
    downloadUrl: string;
}
    
class OrderManager<T extends Order> {
    private orders: T[] = [];

    addOrder(order: T) {
        this.orders.push(order);
    }

    getOrderDetails(orderId: number): T | undefined {
        return this.orders.find(order => order.id === orderId);
    }

    getTotalSales(): number {
        return this.orders.reduce((total, order) => total + (order.quantity * order.price), 0);
    }

    applyDiscount(discount: number): number {
        const totalSales = this.getTotalSales();
        return totalSales * (1 - discount);
    }
}

const physicalOrder: Physical = {
    id: 1,
    product: 'Laptop',
    quantity: 2,
    price: 800,
    deliveryDate: new Date('2024-04-05')
};

const digitalOrder: Digital = {
    id: 2,
    product: 'Software License',
    quantity: 1,
    price: 100,
    downloadUrl: 'http://example.com/download'
};

const physicalOrderManager = new OrderManager<Physical>();
const digitalOrderManager = new OrderManager<Digital>();

physicalOrderManager.addOrder(physicalOrder);
digitalOrderManager.addOrder(digitalOrder);

const orderDetails = physicalOrderManager.getOrderDetails(1);
if (orderDetails) {
    console.log('Detalles de la orden:', orderDetails);
}

console.log('Total de ventas de órdenes físicas:', physicalOrderManager.getTotalSales());
console.log('Total de ventas de órdenes digitales:', digitalOrderManager.getTotalSales());

const discount = 0.1; 
console.log('Total con descuento aplicado:', physicalOrderManager.applyDiscount(discount));
