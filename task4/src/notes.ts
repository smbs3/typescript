import { Note } from './note';
import { User } from './user';
import { log, minLength, required, validate } from './decorators'


interface INote{
  title:string,
  description:string,
  userId:number,
  dueDate: Date

}

export class NotesApp {
  private notes: Note[];
  private users: User[];

  constructor() {
    this.notes = [];
    this.users = [];
  }

  @log
  @validate
  async createUser(@required @minLength(3)name: string, lastname: string, email: string): Promise<void> {
    return new Promise<void>((resolve, reject) => {
        setTimeout(() => {
            const user = new User(name, lastname, email);
            this.users.push(user);
            resolve();
        }, 1000); 
    });
}


  @log
  async createNote( title: string, description: string, userId: number, dueDate: Date): Promise<void> {
    return new Promise<void>((resolve, reject) => {
        setTimeout(() => {
            const note = new Note(title, description, userId, dueDate);
            this.notes.push(note);
            resolve();
        }, 1000); 
    });
}



  getAllNotes(): INote[] {
    return this.notes;
  }

  getAllUsers(): User[] {
    return this.users;
  }
}





// const notesApp = new NotesApp();


// notesApp.createUser('John', 'Doe', 'john.doe@example.com');
// notesApp.createUser('Jane', 'Doe', 'jane.doe@example.com');

// notesApp.createNote('task1', 'Descripción de la nota 1', 1, new Date());
// notesApp.createNote('task2', 'Descripción de la nota 2', 2, new Date());

// console.log('Usuarios:', notesApp.getAllUsers());
// console.log('Notas:', notesApp.getAllNotes());


const notesApp = new NotesApp();


notesApp.createUser('Jan', 'Doe', 'john.doe@example.com')
  .then(() => notesApp.createUser('Jan', 'Doe', 'jane.doe@example.com'))
  .then(() => notesApp.createNote('task1', 'Descripción de la nota 1', 1, new Date()))
  .then(() => notesApp.createNote('task2', 'Descripción de la nota 2', 2, new Date()))
  .then(() => {
    console.log('Usuarios:', notesApp.getAllUsers());
    console.log('Notas:', notesApp.getAllNotes());
  });
