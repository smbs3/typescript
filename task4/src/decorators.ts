import "reflect-metadata";
import * as fs from 'fs';

export function log(target: any, key: string, descriptor: PropertyDescriptor) {
    const originalMethod = descriptor.value;

    descriptor.value = function (...args: any[]) {
        const result = originalMethod.apply(this, args);
        const timestamp = new Date().toISOString();
        const action = `${key}(${args.map(arg => JSON.stringify(arg)).join(',')})`;
        const logMessage = {
            timestamp: timestamp,
            action: action
        };
        fs.appendFileSync('log.json', JSON.stringify(logMessage) + '\n');
        return result;
    };

    return descriptor;
}


const requiredMetadataKey = Symbol("required");
const minLengthMetadataKey = Symbol("minLength");

export function required(
  target: Object,
  propertyKey: string | symbol,
  parameterIndex: number
) {
  let existingRequiredParameters: number[] =
    Reflect.getOwnMetadata(requiredMetadataKey, target, propertyKey) || [];
  existingRequiredParameters.push(parameterIndex);
  Reflect.defineMetadata(
    requiredMetadataKey,
    existingRequiredParameters,
    target,
    propertyKey
  );
}

export function minLength(length: number) {
  return function (
    target: Object,
    propertyKey: string | symbol,
    parameterIndex: number
  ) {
    Reflect.defineMetadata(
      minLengthMetadataKey,
      { index: parameterIndex, length: length },
      target,
      propertyKey
    );
  };
}

export function validate(
  target: any,
  propertyName: string,
  descriptor: TypedPropertyDescriptor<any>
) {
  let method = descriptor.value;
  descriptor.value = function () {
    let requiredParameters: number[] = Reflect.getOwnMetadata(
      requiredMetadataKey,
      target,
      propertyName
    );
    let minLengthRequirements = Reflect.getOwnMetadata(
      minLengthMetadataKey,
      target,
      propertyName
    );

    if (requiredParameters) {
      for (let parameterIndex of requiredParameters) {
        if (
          parameterIndex >= arguments.length ||
          arguments[parameterIndex] === undefined
        ) {
          throw new Error("Missing required argument.");
        }
      }
    }

    if (minLengthRequirements) {
      if (
        arguments[minLengthRequirements.index].length < minLengthRequirements.length
      ) {
        throw new Error(
          `The argument at index ${minLengthRequirements.index} must be at least ${minLengthRequirements.length} characters long.`
        );
      }
    }

    return method.apply(this, arguments);
  };
}

