
function validateEmailFormat(target: any, propertyKey: string) {
  const emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;

  Object.defineProperty(target, propertyKey, {
      get() {
          return this[`_${propertyKey}`];
      },
      set(value) {
          if (!emailRegex.test(value)) {
              throw new Error(`Invalid email format for property ${propertyKey}.`);
          }
          this[`_${propertyKey}`] = value;
      },
      enumerable: true,
      configurable: true
  });
}

function required(target: any, propertyKey: string): void {

  Object.defineProperty(target, propertyKey, {
    get() {
      return this[`_${propertyKey}`];
    },
    set(value) {
      if (!value) {
        throw new Error(`the ${propertyKey} is required .`);
      }
      this[`_${propertyKey}`] = value;
    },
    enumerable: true,
    configurable: true
  });
};

export class User {
  @required
  name: string;

  lastname: string;
  
 @required
  @validateEmailFormat
  email: string;

  constructor(name: string, lastname: string, email: string) {
    this.name = name;
    this.lastname = lastname;
    this.email = email;
  }
}

  
 