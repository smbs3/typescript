interface Book {
    readonly title: string;
    readonly author: string;
  }
  
 const displayBook= (book: Book): void => {
    console.log(book.title);
    console.log(book.author);
  }
  
  const Book: Book = {
    title: "Dragon Ball Z",
    author: "Akira Toriyama",
  };
  

 
  console.log (displayBook(Book));