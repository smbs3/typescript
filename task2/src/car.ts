class Car {
    constructor(public brand: string, public price: number, public model: string) {}
  
    getDetailedInformation(): string {
        return `Brand: ${this.brand}, Model: ${this.model}, Price: $${this.price}`;
    }

    calculatePriceWithTaxes(taxRate: number): number {
        return this.price * (1 + taxRate);
    }
  }
  
  const carDetails = new Car("toyota", 25, 'yaris');
  console.log(carDetails.getDetailedInformation()); 
  console.log(carDetails.calculatePriceWithTaxes(0.15));
  