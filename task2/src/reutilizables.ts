  class Button {
    text: string;
    style: string;
    onClick: () => void;
    size: string;

    constructor(text: string, style: string, onClick: () => void, size: string) {
        this.text = text;
        this.style = style;
        this.onClick = onClick;
        this.size = size;
    }

    click(): void {
        console.log(`Botón '${this.text}' `);
        this.onClick();
    }
}
const onClick = () => {
    return
};


const button1 = new Button("Click me", "default", onClick, "medium");
button1.click();

const button2 = new Button("Submit", "primary", onClick, "small");
button2.click();