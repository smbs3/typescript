
class Engine {
    start(): void {
    }

    stop(): void {
    }
}

class Auto {
    private engine: Engine = new Engine();

    constructor(engine: Engine) {
        this.engine = engine;
    }

    start(): void {
        this.engine.start();
        console.log("Car started");
    }

    stop(): void {
        this.engine.stop();
        console.log("Car stopped");
    }
}


const carEngine = new Engine();
const myCar = new Auto(carEngine);

myCar.start(); 
myCar.stop(); 