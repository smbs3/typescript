interface Container<T> {
    value: T;
}

class DataContainer<T> {

    private items: T[] = [];

    constructor(private value: T) {
        this.items.push(value);
    }

    addElement(item: T): void {
        this.items.push(item);
    }

    getElementByIndex(index: number): T {
        return this.items[index];
    }

    showAllElements(): void {
        console.log(this.items);
    }
}

