interface Dictionary {
    [index: string]: string;
}

class DictionaryClass{
    private items: Dictionary = {};

    addItem(key: string, value: string): void {
        this.items[key] = value;
    }

    getItem(key: string): string | undefined {
        return this.items[key];
    }

    displayItems(): void {
        for (const key in this.items) {
            console.log(`${key}: ${this.items[key]}`);
        }
    }
}

const dictionary = new DictionaryClass();

dictionary.addItem("Person", "persona");
dictionary.addItem("Developer", "Desarrollador");
dictionary.addItem("Junion", "Principiante");

console.log(dictionary.getItem("Developer"));

dictionary.displayItems();
