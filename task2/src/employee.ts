interface Employee {
    name: string;
    surname: string;
  }
  
  interface Developer extends Employee {
    ExperientLevel: string;
  }
  
  interface Designer extends Employee {
    Tools: string;
  }
  
  const developer: Developer = {
    name: "Nick",
    surname: "Pérez",
    ExperientLevel: "MID",
  };
  
  console.log(developer.name);
  console.log(developer.surname);
  console.log(developer.ExperientLevel);
  
  const designer: Designer = {
    name: "lener",
    surname: "garcia",
    Tools: "figma",
  };
  
  console.log(designer.name);
  console.log(designer.surname);
  console.log(designer.Tools);