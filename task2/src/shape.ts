

abstract class Shape {
    abstract calculateArea():number;
    abstract calculatePerimeter():number;
}
  
 
class Circle extends Shape {    
    constructor(public radius: number){
        super()
    };

    calculateArea(): number {
        return Math.PI * Math.pow(this.radius, 2);
    }

    calculatePerimeter(): number {
        return 2 * Math.PI * this.radius;
    }
}

class Rectangle extends Shape {

    constructor(public width: number, public height: number) {
    super()
    }

    calculateArea(): number {
        return this.width * this.height;
    }

    calculatePerimeter(): number {
        return 2 * (this.width + this.height);
    }
}
  