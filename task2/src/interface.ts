interface User {
    name: string;
    surname: string;
    email?: string;
  }
  

 const showCustomMessage = (user: User) =>{
  if(user.hasOwnProperty('email')){
    console.log(`este usuario tiene email ${user.name}`)
  }else
  console.log(`el usuario ${user.name}, no tiene email registrado`)
 }
  
  const userWithEmail: User = {
    name: 'Isaias',
    surname: 'bonilla',
    email: 'isaias.bonilla@gmail.com'
  };
  
  const userWithoutEmail: User = {
    name: 'keyla',
    surname: 'Aguilar'
  };
  
 
 console.log( showCustomMessage(userWithEmail));
// console.log (showCustomMessage(userWithoutEmail));