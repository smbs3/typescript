
class Logger {
    private static instance: Logger;
    private constructor() {}

    static getInstance(): Logger {
        if (!Logger.instance) {
            Logger.instance = new Logger();
        }
        return Logger.instance;
    }
}


const logger1 = Logger.getInstance();
const logger2 = Logger.getInstance();




interface Vehicle {
    drive(): void;
}

class Moto implements Vehicle {
    drive(): void {
        console.log("Driving a moto...");
    }
}
