class Animals {
    constructor(public name: string) {
    }
    eat(): void {
        console.log(`${this.name} is eating`);
    }
  }
  
  class Dogs extends Animals {
    
    constructor(public name: string, public breed: string) {
        super(name);
       
    }

    bark(): void {
        console.log("Woof! Woof!");
    }
  }
  class Cats extends Animals {

    constructor(public name: string, public color: string) {
        super(name);
        
    }

    purr(): void {
        console.log("Purr... Purr...");
    }
  }

  