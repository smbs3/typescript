
interface Animal {
    eat(): void;
    sleep(): void;
  }
  

  class Dog implements Animal {
    eat(): void {
      console.log('El perro come');
    }
  
    sleep(): void {
      console.log('El perro duerme ');
    }
  }
  
  class Cat implements Animal {
    eat(): void {
      console.log('el gato come');
    }
  
    sleep(): void {
      console.log('el gato duerme');
    }
  }
  
 
  const perro = new Dog();
  const gato = new Cat();


perro.eat();
gato.eat();
 