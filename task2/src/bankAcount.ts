class BankAccount {
    private balance: number = 0;
  
    deposit(amount: number) {
      this.balance += amount;
    }
  
    withdrawal(amount: number) {
        if(this.balance> amount){
            this.balance -= amount;
        }else{
            return 'no hay fondo'
        }
    }
  }
  
  const account = new BankAccount();
  account.deposit(1000);
  account.withdrawal(500);