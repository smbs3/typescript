interface EmployeeClass {
    work(task: string): void;
}

class DeveloperClass implements EmployeeClass {


    constructor( public proyect: string) {
        this.proyect = proyect;
    }

    work(task:string): void {
        console.log(`Developer ${this.proyect} is working on ${task}`);
    }
}

class DesignerClass implements EmployeeClass {
  
    proyect: string;

    constructor(proyect: string) {
        this.proyect = proyect;
    }

    work(task: string): void {
        console.log(`Designer ${this.proyect} is designing ${task}`);
    }
}

const assignTask= (proyect: EmployeeClass, task: string): void => {
    proyect.work(task);
}
