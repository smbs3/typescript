"use strict";
const showCustomMessage = (user) => {
    if (user.hasOwnProperty('email')) {
        console.log(`este usuario tiene email ${user.name}`);
    }
    else
        console.log(`el usuario ${user.name}, no tiene email registrado`);
};
const userWithEmail = {
    name: 'Isaias',
    surname: 'bonilla',
    email: 'isaias.bonilla@gmail.com'
};
const userWithoutEmail = {
    name: 'keyla',
    surname: 'Aguilar'
};
console.log(showCustomMessage(userWithEmail));
// console.log (showCustomMessage(userWithoutEmail));
