"use strict";
class DictionaryClass {
    constructor() {
        this.items = {};
    }
    addItem(key, value) {
        this.items[key] = value;
    }
    getItem(key) {
        return this.items[key];
    }
    displayItems() {
        for (const key in this.items) {
            console.log(`${key}: ${this.items[key]}`);
        }
    }
}
const dictionary = new DictionaryClass();
dictionary.addItem("Person", "persona");
dictionary.addItem("Developer", "Desarrollador");
dictionary.addItem("Junion", "Principiante");
console.log(dictionary.getItem("Developer"));
dictionary.displayItems();
