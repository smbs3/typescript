"use strict";
class Car {
    constructor(brand, price, model) {
        this.brand = brand;
        this.price = price;
        this.model = model;
    }
    getDetailedInformation() {
        return `Brand: ${this.brand}, Model: ${this.model}, Price: $${this.price}`;
    }
    calculatePriceWithTaxes(taxRate) {
        return this.price * (1 + taxRate);
    }
}
const carDetails = new Car("toyota", 25, 'yaris');
console.log(carDetails.getDetailedInformation());
console.log(carDetails.calculatePriceWithTaxes(0.15));
