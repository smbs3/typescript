"use strict";
class Animals {
    constructor(name) {
        this.name = name;
    }
    eat() {
        console.log(`${this.name} is eating`);
    }
}
class Dogs extends Animals {
    constructor(name, breed) {
        super(name);
        this.name = name;
        this.breed = breed;
    }
    bark() {
        console.log("Woof! Woof!");
    }
}
class Cats extends Animals {
    constructor(name, color) {
        super(name);
        this.name = name;
        this.color = color;
    }
    purr() {
        console.log("Purr... Purr...");
    }
}
