"use strict";
class BankAccount {
    constructor() {
        this.balance = 0;
    }
    deposit(amount) {
        this.balance += amount;
    }
    withdrawal(amount) {
        if (this.balance > amount) {
            this.balance -= amount;
        }
        else {
            return 'no hay fondo';
        }
    }
}
const account = new BankAccount();
account.deposit(1000);
account.withdrawal(500);
