"use strict";
class DataContainer {
    constructor() {
        this.numberBox = { value: 10 };
        this.stringBox = { value: "Hello, TypeScript!" };
        this.arrayBox = { value: [1, 2, 3, 4, 5] };
        console.log(this.numberBox.value);
        console.log(this.stringBox.value);
        console.log(this.arrayBox.value);
    }
}
const container = new DataContainer();
