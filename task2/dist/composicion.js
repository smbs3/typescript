"use strict";
class Engine {
    start() {
    }
    stop() {
    }
}
class Auto {
    constructor(engine) {
        this.engine = new Engine();
        this.engine = engine;
    }
    start() {
        this.engine.start();
        console.log("Car started");
    }
    stop() {
        this.engine.stop();
        console.log("Car stopped");
    }
}
const carEngine = new Engine();
const myCar = new Auto(carEngine);
myCar.start();
myCar.stop();
