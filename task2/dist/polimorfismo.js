"use strict";
class DeveloperClass {
    constructor(proyect) {
        this.proyect = proyect;
        this.proyect = proyect;
    }
    work(task) {
        console.log(`Developer ${this.proyect} is working on ${task}`);
    }
}
class DesignerClass {
    constructor(proyect) {
        this.proyect = proyect;
    }
    work(task) {
        console.log(`Designer ${this.proyect} is designing ${task}`);
    }
}
const assignTask = (proyect, task) => {
    proyect.work(task);
};
