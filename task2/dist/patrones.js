"use strict";
class Logger {
    constructor() { }
    static getInstance() {
        if (!Logger.instance) {
            Logger.instance = new Logger();
        }
        return Logger.instance;
    }
}
const logger1 = Logger.getInstance();
const logger2 = Logger.getInstance();
class Moto {
    drive() {
        console.log("Driving a moto...");
    }
}
