"use strict";
class Dog {
    eat() {
        console.log('El perro come');
    }
    sleep() {
        console.log('El perro duerme ');
    }
}
class Cat {
    eat() {
        console.log('el gato come');
    }
    sleep() {
        console.log('el gato duerme');
    }
}
const perro = new Dog();
const gato = new Cat();
perro.eat();
gato.eat();
