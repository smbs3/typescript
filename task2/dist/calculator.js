"use strict";
const calculator = {
    sum: (num1, num2) => {
        return num1 + num2;
    },
};
console.log(calculator.sum(5, 3));
const operate = (calculator, num1, num2) => {
    const result = calculator.sum(num1, num2);
    console.log(`La suma de ${num1} y ${num2} es ${result}`);
};
console.log(operate(calculator, 7, 4));
