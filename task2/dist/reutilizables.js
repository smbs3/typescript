"use strict";
class Button {
    constructor(text, style, onClick, size) {
        this.text = text;
        this.style = style;
        this.onClick = onClick;
        this.size = size;
    }
    click() {
        console.log(`Botón '${this.text}' `);
        this.onClick();
    }
}
