import { ValidateLength } from '../decorators/validatelength';

export class Employee {
  @ValidateLength(5)
  name: string;
  age: number;
  role: string;

  constructor(name: string, age: number, role: string) {
    this.name = name;
    this.age = age;
    this.role = role;
  }
}