import { Employee } from "./employee";

export class Intern extends Employee {
  intern: string;

  constructor(name: string, age: number, role: string, intern: string) {
    super(name, age, role);
    this.intern = intern;
  }
}