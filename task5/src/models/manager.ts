import { Employee } from "./employee";

export class Manager extends Employee {
  department: string;

  constructor(name: string, age: number, role: string, department: string) {
    super(name, age, role);
    this.department = department;
  }
}