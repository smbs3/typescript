import { Employee } from "../models/employee";
import { Manager } from "../models/manager";
import { Intern } from "../models/intern";

export async function fetchEmployeeData(): Promise<Employee[]> {
  return new Promise((resolve) => {
    setTimeout(() => {
      resolve([
        new Manager("John Doe", 30, "Manager", "IT"),
        new Intern("Jane Doe", 22, "Intern", "Harvard"),
      ]);
    }, 1000);
  });
}
console.log("Generating employee data to verify operation...");
fetchEmployeeData().then((employees) => {
  console.log("Employee data generated: ", employees);
});

