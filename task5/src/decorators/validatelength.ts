export  function ValidateLength(length: number) {
    return function (target: any, propertyKey: string): void {
  
      Object.defineProperty(target, propertyKey, {
        get() {
          return this[`_${propertyKey}`];
        },
        set(value) {
          if (value.length < length) {
            throw new Error(`the ${propertyKey}  must be greater than ${length} . `);
          }
          this[`_${propertyKey}`] = value;
        },
        enumerable: true,
        configurable: true
      });
    };
  }