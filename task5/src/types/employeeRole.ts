import { Manager } from "../models/manager";
import { Intern } from "../models/intern";

export type EmployeeRole = Manager | Intern;