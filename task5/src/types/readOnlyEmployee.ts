import { Employee } from "../models/employee";

export type ReadOnlyEmployee = { readonly [K in keyof Employee]: Employee[K] };