import { Employee } from "../models/employee";

export type EmployeeWithSalary = Employee & { salary: number };