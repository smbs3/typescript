type HandleNumber=(value:number)=>number;

const handleNumber = (value:number): HandleNumber =>{
    const mult=(multiple:number): number=>{
        return multiple*value;
    }
    return mult;
}
const multiples = handleNumber(10);
console.log(multiples(5));