"use strict";
const handleNumber = (value) => {
    const mult = (multiple) => {
        return multiple * value;
    };
    return mult;
};
const multiples = handleNumber(10);
console.log(multiples(5));
